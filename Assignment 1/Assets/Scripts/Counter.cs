﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    private int score;
    public Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        SetScoreText();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Duck"))
            score = score + 1;
        SetScoreText();
    }

    void SetScoreText()
    {
        scoreText.text = "Score: " + score.ToString();
    }

    void OnTriggerEnter2D(Collision2D gameobject)
    {
        if (gameobject.collider.tag == "Duck")
        {
            Debug.Log("Duck Down!");
        }
    }
}
