﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Asteroid : MonoBehaviour



{
    public GameObject Duck;
    public GameObject AsteroidShoot;

    public Vector3 speed = new Vector3(7f, 0f, 0f);

    public float horizontalPress;
    public float horizontalPush;

    public Transform Spawnpoint;

    private int score;
    public Text scoreText;

    Rigidbody2D rigidbody2DComponent;

    void Awake()
    {
       rigidbody2DComponent = GetComponent<Rigidbody2D>();  
    }

    // Start is called before the first frame update
    void Start()
    {
        speed = new Vector3(7f, 0f, 0f);

        score = 0;
        SetScoreText();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
       if (gameObject.CompareTag("Duck"))
        score = score + 1;
        SetScoreText();
    }

    void SetScoreText()
    {
        scoreText.text = "Score: " + score.ToString();
    }

    private void FixedUpdate()
    {
        //Keeps the asteroid in place upon spawn
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, -5);

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.Scale(speed, new Vector3(Input.GetAxis("Horizontal"), 7f)) * Time.deltaTime);

        //Input for horizontal movement
        horizontalPress = Input.GetAxis("Horizontal");

        rigidbody2DComponent.AddForce(new Vector3(horizontalPress * horizontalPush, 0));
    }

    private void OnCollisionEnter2D(Collision2D gameobject)
    {
        //When this class object touches the "Counter" platforms it is destroyed
        if (gameobject.collider.tag == "Counter")
        {
            Debug.Log("Hit!");
        }

        if (gameobject.collider.tag == "Counter")
        {
            this.transform.position = new Vector3(0, 4.92f, 0);
        }
    }

    private void InstantiateThis()
    {
        Instantiate(AsteroidShoot, Vector3.zero, Quaternion.identity);
    }

}
